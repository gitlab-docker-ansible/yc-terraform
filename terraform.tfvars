network_name = "test-network-1"
subnet = ["10.2.0.0/24"]
zone = "ru-central1-a"
auto_delete = true
image_id = "fd8kb72eo1r5fs97a1ki"
disk_size = 10
instance_name = "server-1"
platform_id = "standard-v2"