terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}



provider "yandex" {
  # token     = ""
  # cloud_id  = ""
  # folder_id = ""
  zone      = "${var.zone}"
}

# Создаем сеть
resource "yandex_vpc_network" "test-network" {
  name = "${var.network_name}"
  description = "test Network"
}
# Создаем подсеть 
resource "yandex_vpc_subnet" "test-subnet-a" {
  v4_cidr_blocks = "${var.subnet}"
  zone           = "${var.zone}"
  network_id     = "${yandex_vpc_network.test-network.id}"
  depends_on = [
    yandex_vpc_network.test-network
  ]
}

# platform_id
# Intel Broadwell (standard-v1)	Intel® Xeon® Processor E5-2660 v4	32	2.00
# Intel Cascade Lake (standard-v2)	Intel Xeon Gold 6230	80	2.10
# Intel Ice Lake (standard-v3)	Intel Xeon Gold 6338	96	2.00

resource "yandex_compute_instance" "vm-1" {
  name        = "${var.instance_name}"
  platform_id = "${var.platform_id}"
  zone        = "${var.zone}"


  resources {
    core_fraction = 5 # Доля vCPU
    cores         = 2 # vCPU
    memory        = 2 # RAM
  }

  boot_disk {
    auto_delete = "${var.auto_delete}" #(Optional) Defines whether the disk will be auto-deleted when the instance is deleted. The default value is True.
    initialize_params {
      image_id = "${var.image_id}" # Ubuntu 20.04
      size = "${var.disk_size}"
    }
  }
  

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    user-data = "${file("user-meta.txt")}"
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.test-subnet-a.id}"
    nat       = true
  }

  depends_on = [
    yandex_vpc_subnet.test-subnet-a
  ]

}


data "yandex_compute_instance" "my_instance" {
  instance_id = "${resource.yandex_compute_instance.vm-1.id}"
}

output "instance_external_ip" {
  value = "${data.yandex_compute_instance.my_instance.network_interface.0.nat_ip_address}"
  
}