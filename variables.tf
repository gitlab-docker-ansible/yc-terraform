variable "network_name" {
  type = string
  default = "default_network"
  description = "YC network"
}

variable "subnet" {
  type = list
  default = ["10.2.0.0/16"]
  description = "Subnet for VMs"
}

variable "zone" {
  type = string
  default = "ru-central1-a"
}

variable "auto_delete" {
  type = bool
  default = true
  description = "Defines whether the disk will be auto-deleted when the instance is deleted. The default value is True"
}

variable "image_id" {
  type = string
  default = "fd8kb72eo1r5fs97a1ki"
  description = "Image on instance Ubuntu 20.04"
}

variable "disk_size" {
  default = 10
  description = "Instance disk size"
}

variable "instance_name" {
  type = string
  default = "test-server"
  description = "Instance name"
}

variable "platform_id" {
  type = string
  default = "standard-v2"
  description = "platform"
}